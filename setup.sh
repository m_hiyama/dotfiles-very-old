# 

DOTFILES=~/configs/dots

if [ -f $DOTFILES/.bashrc ]; then
    rm -f ~/.bashrc
    uname | grep MINGW > /dev/null
    if [ $? = 0 ]; then
	cp $DOTFILES/.bashrc ~
    else
	ln -s $DOTFILES/.bashrc ~
    fi
fi
