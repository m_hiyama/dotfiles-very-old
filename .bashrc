echo 'This is ~/.bashrc'
# CONFIGS_DIR --> DOTFILES

if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

DOTFILES=~/configs/dots
if [ -f $DOTFILES/.bashrc.common ]; then
    echo loading  $DOTFILES/.bashrc.common
    source $DOTFILES/.bashrc.common
else
    echo NOT exist $DOTFILES/.bashrc.common
fi

THIS_HOST=`hostname`

echo "This Host = $THIS_HOST"
if [ -f $DOTFILES/.bashrc.$THIS_HOST ]; then
    echo loading  $DOTFILES/.bashrc.$THIS_HOST
    source $DOTFILES/.bashrc.$THIS_HOST
else
    echo NOT exist $DOTFILES/.bashrc.$THIS_HOST
fi

